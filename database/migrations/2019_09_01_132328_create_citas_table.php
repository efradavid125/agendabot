<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('hora');
            $table->dateTime('fecha_notificacion');
            $table->dateTime('fecha_cita');
            $table->integer('cancelado');
            $table->timestamps();

            $table->unsignedBigInteger('agenda_id');

            $table->foreign('agenda_id')
                ->references('id')
                ->on('agendas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->unsignedBigInteger('destinatario_id');

            $table->foreign('destinatario_id')
                ->references('id')
                ->on('destinatarios')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}

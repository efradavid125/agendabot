
require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('tareas', require('./components/TareasComponent.vue').default);

//*******/layout*******
Vue.component('sidebar', require('./components/layout/SidebarComponent.vue').default);
Vue.component('topbar', require('./components/layout/TopBarComponent.vue').default);

//*******/layout*******



const app = new Vue({
    el: '#app',
});

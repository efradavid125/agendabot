@extends('layouts.app')

@section('content')
<div class="container">
    
    @if(Auth::user()->hasRole('user') || Auth::user()->hasRole('admin') )
       <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Administrar Notas</div>

                    <div class="card-body" >
                        <tareas/>
                    </div>
                </div>
            </div>
        </div> 
    @else
        <div>Acceso usuario</div>
    @endif
    
</div>
@endsection

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="css/sb-admin-2.css" rel="stylesheet">


<!-- aqui van los link de css y js compilados desde el frontend -->

    <link href=js/NuevaAgenda.js rel=prefetch>
    <link href=js/about.js rel=prefetch>
    <link href=css/app.css rel=preload as=style>
    <link href=js/app.js rel=preload as=script>
    <link href=js/chunk-vendors.js rel=preload as=script>
    <link href=css/app.css rel=stylesheet>
</head>
<body id="page-top" >

@if (Auth::check())
    <div id="app">
    </div>
    <script>
        const user = { 
            token:'{{Auth::user()->token}}',
            rol:'admin'
        }
    </script>
@endif
    
   
    <script src=jquery/jquery.min.js></script>
    <script src=bootstrap/js/bootstrap.bundle.min.js></script>
    <script src=js/chunk-vendors.js></script>
    <script src=js/app.js></script>


    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
        
</body>
</html>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    public function destinatario()
    {
        return $this->hasOne('App\Destinatario');
    }
}

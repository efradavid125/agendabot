<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function Fecha_Hora_Mes(){

     
         date_default_timezone_set('Chile/Continental');
     
         $fecha_hora = date("Y-m-d G:i:s");
     
         $fecha = date("Y-m-d");
     
         $mes = date("m");
     
         $array =  array('fecha_hora' => $fecha_hora,
                       'fecha' => $fecha,
                       'mes' => $mes
                     );   
     
       
         return $array;
     }

     private function Envio(){

       $instanceId = '1111';

       $token = 'sss';

        $data = [
            'phone' => $info['numero'], // Receivers phone
            'body' => $texto['texto'], // Message
        ];
        $json = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        $url = 'https://api.chat-api.com/instance'.$instanceId.'/sendMessage?token='.$token;
        // Make a POST request
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json
            ]
        ]);
        // Send a request
        $result = file_get_contents($url, false, $options);
     }

    public function Email($info){

          Mail::send('emails'.$info['email'], $data, function($message) use ($data) {
            $message->to('noreply@agendabot', $data['name'])->subject($data['asunto']);
        });
    }
    
    public function Valida_Rut($rut){

        //$rut = $_POST['rut']; //recibo rut
        $rut_sin_puntos = str_replace('.', "", $rut); //elimino puntos
        $numerosentrada = explode("-", $rut_sin_puntos); //separo rut de dv
        $verificador = $numerosentrada[1]; //asigno valor de dv
        $numeros = strrev($numerosentrada[0]);  //separo rut de dv
        $count = strlen($numeros); //asigno la longitud del string en este caso 8
        $count = $count -1; //resto uno al contador para comenzar mi ciclo ya que las posiciones empiezan de 0
        $suma = 0;
        $recorreString = 0;
        $multiplo = 2;
        for ($i=0; $i <=$count ; $i++) {   //inicio ciclo hasta la posicion 7
            $resultadoM = $numeros[$recorreString]*$multiplo; // recorro string y multiplico 
    
            $suma = $suma + $resultadoM; // se suma resultado de multiplicacion por ciclo
            if ($multiplo == 7) { 
                $multiplo = 1;
            }
            $multiplo++;
            $recorreString++;
        }
        $resto = $suma%11;
        $dv= 11-$resto;
        if ($dv == 11) {
            $dv = 0;
        }
        if ($dv == 10) {
            $dv = K;
        }
        if ($verificador == $dv) {

          return true;

        }else {

          return false;
        }
    }
}
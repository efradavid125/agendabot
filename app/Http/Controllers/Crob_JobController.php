<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Citas;

class Crob_JobController extends Controller
{

    /**
     * devuelve informacion del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  

        $link = "www.agendabot.cl/confirma/";

        $fecha = $this->Fecha_Hora_Mes();

      /*  $cliente = Cita::
        join('cliente', 'cliente.id', '=', 'citas.cliente_id')
        ->join('agendas', 'agendas.id', '=', 'citas.agenda_id')
        ->join('agenda_user', 'agenda_user.agenda_id', '=', 'agendas.id')
        ->join('agenda_user', 'agenda_user.user_id', '=', 'users.id')
        ->where('citas.fecha_notificacion',$fecha['fecha'])
        ->where('citas.cancelado',1)
        ->get();
     */

        $citas = Cita::
        join('agendas', 'agendas.id', '=', 'citas.agenda_id')
        ->where('citas.fecha_notificacion',$fecha['fecha'])
        ->where('citas.cancelado',1)
        ->get();

        foreach ($citas as $key) {

            $info = "*Confirmación*\n\n".$key['descripcion']."\n\n Fecha Cita:".$key['fecha_cita']."\n\n Hora Cita:".$key['hora'];

            $info = $info."\n\nSolicitamos confirmar su cita pinchando en el siguiente link: ".$link.$key['token_cita'];
          
            $this->Envio($info);

        }
        
    }

  
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * devuelve informacion del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  

    
        //validamos si viene vacio
        if(empty($request->identificador) ){

            $msn['estado'] = false;

            $msn['msn'] = 'Debe ingresar la identificacion';

            return $msn;

        }
        //validamos el rut chileno
        if(!$this->Valida_Rut($request->identificador)){

            $msn['estado'] = false;

            $msn['msn'] = 'Identificador no es correcto. Formato: 14509303-1';

            return $msn;  

        }

        $cliente = Cliente::
                    where('identificador', $request->identificador )
                    ->firts();
            
        if($cliente->isNotEmpty()){

                $msn['estado'] = true;

                $msn['msn'] = $cliente;

        }else{

                $msn['estado'] = false;

                $msn['msn'] = 'No existe información';     
        }
            
        return $msn;    
    }
    

    //validamos si existe el cliente.
    public function Cliente(Request $request){

        $cliente = Cliente::where('identificador',$request->identificador)->first();

        return $cliente->isNotEmpty() ? $this->store($request) : $this->update($request);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    private function store($request)
    {   
       
        $cliente = new Cliente();

        $cliente->id_agenda = $request->id_agenda;

        $cliente->identificador = $request->identificador;

        $cliente->whatsapp = $request->whatsapp;

        $cliente->email = $request->email;

        $cliente->save();

        if($cliente){

        }

        return $cliente;
    }

   
    private function update($request)
    {
        $cliente = Cliente::where('id_agenda',  $request->id_agenda)->find($request->identificador);

        $cliente->nombre = $request->nombre;

        $cliente->descripcion = $request->descripcion;

        $cliente->save();
       

    }

}
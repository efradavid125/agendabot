<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use App\User;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        return User::where('token',$request->token)->first()->agendas;
    }

    public function Select(){

        return Agenda::orderBy('id','ASC')
        ->join('agenda_user', 'agenda_user.id', '=', 'agenda.agenda_id')
        ->where('agenda_user.user_id','=', Auth::user()->id)
        ->pluck('agenda.nombre', 'id');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agenda = new Agenda();

        $agenda->nombre = $request->nombre;

        $agenda->descripcion = $request->descripcion;

        $agenda->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $agenda = Agenda::find($request->id);

        $agenda->nombre = $request->nombre;

        $agenda->descripcion = $request->descripcion;

        $agenda->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //ver si la agenda se pued eliminar
        $agenda = Agenda::find($id);
        
        $agenda->delete();
    }
}

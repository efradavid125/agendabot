<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class AdministracionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * devuelve informacion del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        return Users::orderBy('id','ASC')
        ->where('rol_id','=', 2)
        ->get();
   
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function show(Request $request)
    {   
        
        return User::where('token',$request->token)->get()->agendas;
      
    }


    public function update(Request $request)
    {

        $users = Users::find($request->id);

        $users->estado = $request->estado;

        $users->save();

         //obtenemoos los datos del usuario
         $us = Users::where('id',$request->id)
         ->firts();
         //enviamos una notificacion asu email
         $formato['para'] = $us->email;
 
         $formato['name'] = $us->name;
 
         $formato['data'] = $us;
 
         $formato['asunto'] = 'Bloquedo de cuenta';
 
         $formato['vista'] = 'emails.bloqueo_cuenta';
 
         $this->Email($formato);

    }


  
}
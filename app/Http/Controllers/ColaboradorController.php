<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Role_User;
use App\Agenda_User;


class ColaboradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   

        return User::select('user.email','user.name', 'agenda.nombre', 'agenda.id')
        ->join('agenda_user', 'agenda_user.id', '=', 'agenda.agenda_id')
        ->join('users', 'users.id', '=', 'agenda_user.user_id')
        ->where('user.id','=', Auth::user()->id)
        ->get();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        //creamos el usuario
        $user = User::create([
            'name' => $request['name'],

            'email' => $request['email'],

            'password' => Hash::make('12345678'),

            'token' =>  Hash::make($request['email']),

            'profesions_id' => 0,
        ]);

        //creamos el rol del usuario
        $role_user = new Role_User();

        $role_user->role_id = 1; //rol colaborador

        $role_user->user_id = $user->id;

        $role_user->save();

        //creamos el usuario con la agenda

        $agenda_user = new Agenda_User();

        $agenda_user->agenda_id = $request['agenda_id']; 

        $agenda_user->user_id = $user->id;

        $agenda_user->save();

         //enviamos una notificacion asu email

         $request['clave'] = '12345678';

         $formato['para'] = $request['email'];
 
         $formato['name'] = $request['name'];
 
         $formato['data'] = $request;
 
         $formato['asunto'] = 'Nueva cuenta creada';
 
         $formato['vista'] = 'emails.nueva_cuenta_colaborador';
 
         $this->Email($formato);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //actualizamos el email 
       $user = Users::find($request->users_id);

       $user->email = $request->email;

       $user->save();

       //actualizamos la agenda el colaborador
       Agenda_User::where('agenda_id', $request->agenda_id)
       ->where('user_id','=', $request->users_id)
       ->update( ['agenda_id'=> $request->agenda_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agenda_User::where('agenda_id', $request->agenda_id)
        ->where('user_id','=', $request->users_id)
        ->delete();
    }
}
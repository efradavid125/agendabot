<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cita;

class CitaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * devuelve informacion del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        return  Cita::where('agenda_id',$request->id_agenda)->
        where('fecha_cita',$request->fecha_cita)
        ->get();
      
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store($request)
    {   
        
        $fecha_notificacion = date("d-m-Y",strtotime($request->fecha_notificacion."- 1 days"));

        $cita = new Cita();

        $cita->agenda_id = $request->id_agenda;

        $cita->destinatario_id = $request->destinatario_id;

        $cita->hora = $request->hora;
        
        $cita->fecha_notificacion = $fecha_notificacion;

        $cita->fecha_cita = $request->fecha_cita;

        $cita->cancelado = 0;

        $cita->save();

        //obtenemoos los datos del cliente
        $cliente = Cliente::where('destinatario_id',$request->destinatario_id)
        ->firts();
        //enviamos una notificacion asu email
        $formato['para'] = $cliente->email;

        $formato['name'] = $cliente->nombre;

        $formato['data'] = $cliente;

        $formato['asunto'] = 'Nueva cita';

        $formato['vista'] = 'emails.nueva_cita';

        $this->Email($formato);
    }


    public function update($request)
    {

        $fecha_notificacion = date("d-m-Y",strtotime($request->fecha_notificacion."- 1 days"));


        $cita = Cita::find($request->id);

        $cita->agenda_id = $request->id_agenda;

        $cita->destinatario_id = $request->destinatario_id;

        $cita->hora = $request->hora;

        $cita->fecha_notificacion = $fecha_notificacion;

        $cita->fecha_cita = $request->fecha_cita;

        $cita->cancelado = 0;

        $cita->save();
       

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cita = Cita::find($id);
        
        $cita->delete();
    }

    public function Cancelacion(Request $request){

        if(!empty($request->citas)){

             foreach ($request->citas as $key ) {

                $cita = Cita::find($request->id);

                $cita->cancelado = 1;
            
                $cita->save();

                 //obtenemoos los datos del cliente
                $cliente = Cita::select('cliente.nombre', 'cliente.email')
                ->where('cita.id',$request->id)
                ->join('agendas', 'agendas.id', '=', 'citas.agenda_id')
                ->join('cliente', 'cliente.id', '=', 'citas.cliente_id')
                ->get();

                //enviamos una notificacion asu email
                $formato['para'] = $cliente->email;

                $formato['name'] = $cliente->nombre;

                $formato['data'] = $cliente;

                $formato['asunto'] = 'Cita cancelada';

                $formato['vista'] = 'emails.cita_cancela';

                $this->Email($formato);
                
             }  
             
           
             $msn['estado'] = true;

             $msn['msn'] = 'Se cancelaron las citas seleccionadas. Se envio aviso a clientes.';
 
             return $msn;

        }else{

            $msn['estado'] = false;

            $msn['msn'] = 'Debe seleccionar citas a cancelar';

            return $msn;
        }
    }

  
}
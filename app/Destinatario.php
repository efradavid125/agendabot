<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    public function cita()
    {
        return $this->belongsTo('App\Cita');
    }
}

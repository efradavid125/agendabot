<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    redirect('/app');
});

Auth::routes();

Route::resource('/notas', 'NotaController')->middleware('auth');
Route::resource('/agendas', 'AgendaController');

Route::get('/app', 'HomeController@index')->name('app');
